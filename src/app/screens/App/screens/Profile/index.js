import Screen from './components/Screen.vue'
import Default from './screens/Default'
import OrderLists from './screens/OrderLists'

export default {
  children: [ Default, OrderLists ],
  path: '/perfil',
  component: Screen
}
