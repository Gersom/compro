import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const updatePreviousPath = ({commit}, path) => {
  commit(types.UPDATE_PREVIOUS_PATH, path)
}
