import Screen from './components/Screen'
import Default from './screens/Default'
import ServiceProfile from './screens/ServiceProfile'

export default {
  children: [
    Default,         // path: ''
    ServiceProfile   // path: ':serviceSlug'
  ],
  component: Screen,
  path: '/servicios'
}
