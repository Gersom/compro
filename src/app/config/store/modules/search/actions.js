import axios from 'axios'
import {camelizeKeys} from 'humps'

import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const loadSearch = ({ commit }, quest) => {
  commit(types.BEGIN_AJAX_CALL)

  axios.get(`${window.API_URL}/search?name=${quest}`)
    .then(({data}) => {
      commit(types.RECEIVE_RESPONSE, camelizeKeys(data.data))
      commit(types.RECEIVE_REQUEST, quest)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}

export const loadPreview = ({ commit }, quest) => {
  commit(types.BEGIN_AJAX_CALL)

  axios.get(`${window.API_URL}/search?q=4&name=${quest}`)
    .then(({data}) => {
      commit(types.RECEIVE_PREVIEW, camelizeKeys(data.data))
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}
