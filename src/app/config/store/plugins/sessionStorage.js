import * as keys from './constantsKeys'
import * as oauth from 'store/modules/oauth/types'
import * as cart from 'store/modules/cart/types'

export function sessionStoragePlugin (store) {
  store.subscribe(({type}, {
    oauth: {token},
    cart: {added}
  }) => {
    switch (type) {
      case oauth.UPDATE_TOKEN:
      case oauth.REMOVE_TOKEN:
        window.localStorage.setItem(keys.OAUTH_STORAGE_KEY, JSON.stringify(token))
        break
      case cart.ADD_NEW_ITEM:
      case cart.CHANGE_QUANTITY:
      case cart.INCREASE_QUANTITY:
      case cart.ADD_NEW_FEATURE:
      case cart.UPDATE_PRICE_ID_CART:
      case cart.UPDATE_PRICE_CART:
      case cart.UPDATE_ATTRIBUTES_CART:
      case cart.REMOVE_FEATURE_CART:
      case cart.REMOVE_PRODUCT_CART:
      case cart.CHECKOUT_REQUEST:
      case cart.CHECKOUT_FAILURE:
      case cart.CLEAR_CART:
        window.localStorage.setItem(keys.CART_STORAGE_KEY, JSON.stringify(added))
        break
      default:
        //
    }
  })
}
