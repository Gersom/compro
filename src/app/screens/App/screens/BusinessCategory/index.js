import Screen from './components/Screen.vue'
import Default from './screens/Default'
import Category from './screens/Category'

export default {
  children: [
    Default,
    Category
  ],
  path: '/c/:business_category',
  component: Screen
}
