export const searchRequest = (state) => {
  return state.request
}

export const searchResponse = (state) => {
  return state.response
}

export const searchPreview = (state) => {
  return state.preview
}
