import AssociatesScreen from './components/Screen'

export default {
  path: '/socios',
  name: 'Associates',
  component: AssociatesScreen
}
