import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import ajax from './modules/ajax'
import cart from './modules/cart'
import categories from './modules/categories'
import currencies from './modules/currencies'
import itemDetails from './modules/itemDetails'
import items from './modules/items'
import modal from './modules/modal'
import routesModule from './modules/routesModule'
import search from './modules/search'
import news from './modules/news'
import oauth from './modules/oauth'
import tags from './modules/tags'
import user from './modules/user'

import {sessionStoragePlugin} from './plugins/sessionStorage'
import {notificationPlugin} from './plugins/notification'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    ajax,
    cart,
    categories,
    currencies,
    itemDetails,
    items,
    modal,
    routesModule,
    search,
    news,
    oauth,
    tags,
    user
  },
  strict: debug,
  plugins: debug ? [createLogger(), sessionStoragePlugin, notificationPlugin] : [sessionStoragePlugin, notificationPlugin]
})
