// IMPORTS
import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
import axios from 'axios'
let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
export const beginUser = ({ commit, rootState }) => {
  commit(types.BEGIN_AJAX_CALL)

  axios({
    method: 'get',
    url: `${window.API_URL}/me`,
    headers: {
      'Authorization': `Bearer ${rootState.oauth.token.access}`,
      'Content-Type': 'application/json'
    }
  })
  .then(({ data }) => {
    commit(types.BEGIN_USER, data.data)
    commit(types.AJAX_CALL_SUCCESS)
  })
  .catch(({response}) => {
    commit(types.AJAX_CALL_ERROR)
  })
}
