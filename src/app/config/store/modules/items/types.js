// mutations
export const RECEIVE_ITEMS = 'RECEIVE_ITEMS'
export const ADD_ITEMS = 'ADD_ITEMS'
export const CLEAN_ITEMS = 'CLEAN_ITEMS'

export const UPDATE_RANGE_MIN = 'UPDATE_RANGE_MIN'
export const UPDATE_RANGE_MAX = 'UPDATE_RANGE_MAX'
export const UPDATE_PAGE = 'UPDATE_PAGE'
