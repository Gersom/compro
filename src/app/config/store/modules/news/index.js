import * as types from './types'
// import * as defaults from './defaults'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: [{
    day: '12',
    id: 1,
    imageUrl200px: 'https://dl.dropboxusercontent.com/s/fe87izypxdv7eel/cobre200.png?dl=0',
    image_url: 'http://mercadosyregiones.com/wp-content/uploads/2017/03/Cobre.jpg',
    month: 'Jun',
    slug: 'undefined',
    title: '',
    year: ''
  }],
  last: {
    slug: 'notice-default'
  },
  profile: {
    title: '',
    day: '',
    month: '',
    year: '',
    content: ''
  },
  partners: [],
  related: [],
  advert: {
    imageUrl: '',
    url: '#'
  }
}

const mutations = {
  [types.FILL_NEWS] (state, elements) {
    state.all = elements
  },
  [types.RELATED_NOTICE] (state, element) {
    state.related = element
  },
  [types.FILL_NOTICE_PROFILE] (state, element) {
    state.profile = element
  },
  [types.FILL_PARTNERS] (state, elements) {
    state.partners = elements
  },
  [types.FILL_ADVERT] (state, element) {
    state.advert = element
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
