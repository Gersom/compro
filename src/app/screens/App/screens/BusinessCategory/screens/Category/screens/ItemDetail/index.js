import Screen from './components/Screen.vue'

export default {
  component: Screen,
  name: 'ItemDetail',
  path: ':product_slug'
}
