import axios from 'axios'
import {camelizeKeys} from 'humps'

import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const loadCategories = ({commit}) => {
  commit(types.BEGIN_AJAX_CALL)

  axios.get(`${window.API_URL}/business-categories`)
    .then(({data}) => {
      commit(types.RECEIVE_CATEGORIES, camelizeKeys(data.data))
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}
