import Screen from './components/Screen'

export default {
  path: '/productos/:productSlug',
  name: 'Products',
  component: Screen
}
