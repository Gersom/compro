import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: [],
  selectedId: 1
}

const mutations = {
  [types.UPDATE_CURRENCIES] (state, currencies) {
    state.all = currencies
  },
  [types.CHANGE_CURRENCY_SELECTED] (state, select) {
    state.selectedId = select
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
