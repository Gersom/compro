import * as actions from './actions'
// import * as defaults from './defaults'
import * as getters from './getters'
import * as types from './types'

const state = {
  data: {}
}

const mutations = {
  [types.BEGIN_USER] (state, element) {
    state.data = element
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
