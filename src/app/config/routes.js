import Associates from 'App/screens/Associates'
import Blog from 'App/screens/Blog'
import BusinessCategory from 'App/screens/BusinessCategory'
import Catalog from 'App/screens/Catalog'
import Home from 'App/screens/Home'
import Lists from 'App/screens/Lists'
import LogIn from 'App/screens/LogIn'
import Malls from 'App/screens/Malls'
import Map from 'App/screens/Map'
import Order from 'App/screens/Order'
import Products from 'App/screens/Products'
import Profile from 'App/screens/Profile'
import Search from 'App/screens/Search'
import Services from 'App/screens/Services'
import SignIn from 'App/screens/SignIn'

import Shops from 'App/screens/Shops'
import ShopProfile from 'App/screens/ShopProfile'

const routes = [
  Associates,         // path: '/socios',
  Blog,               // path: '/blog',
  BusinessCategory,   // path: '/c/:business_category',
  Catalog,            // path: '/catalogo',
  Home,               // path: '/',
  Lists,              // path: '/listas/:listCode',
  LogIn,              // path: '/ingresar',
  Malls,              // path: '/tacna',
  Map,                // path: '/mapa',
  Order,              // path: '/orden',
  Products,           // path: '/products/:productSlug',
  Profile,            // path: '/perfil',
  Search,             // path: '/busqueda',
  Services,           // path: '/servicios',
  SignIn,             // path: '/registrar',

  Shops,              // path: '/tiendas',
  ShopProfile         // path: '/:shopSlug',
]

export default routes
