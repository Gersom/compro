import Screen from './components/Screen'
import Default from './screens/Default'

import MapRegister from './screens/MapRegister'
import MapWelcome from './screens/MapWelcome'

export default {
  children: [
    Default,       // path: ''
    MapRegister,   // path: 'registro'
    MapWelcome     // path: 'bienvenido'
  ],
  component: Screen,
  path: '/mapa'
}
