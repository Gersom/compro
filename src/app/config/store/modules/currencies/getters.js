export const currencies = (state) => {
  return state.all
}

export const currencySelected = (state) => {
  let jsonTmp
  let all = state.all
  let selected = state.selectedId
  let withData = false
  let tmp = {
    code: 'PEN',
    exchangeRate: 200,
    id: 1,
    name: 'Nuevo Sol',
    symbol: 'S/.'
  }

  for (var i = 0; i < all.length; i++) {
    if (all[i].id === selected) {
      jsonTmp = all[i]
      withData = true
      break
    }
  }

  return withData ? jsonTmp : tmp
}
