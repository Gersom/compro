import CategoryScreen from './components/Screen'
import Default from './screens/Default'
// import ItemDetail from './screens/ItemDetail'

export default {
  children: [
    Default
    // ItemDetail
  ],
  path: ':category',
  component: CategoryScreen
}
