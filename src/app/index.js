// imports modules || importación de modulos
import Vue from 'vue'
import { sync } from 'vuex-router-sync'

// register components || registro de componentes
import registerComponents from './config/registerComponents'
registerComponents(Vue)

// import libraries || importacion de bibliotecas
import router from './config/vue-router'
import store from 'store'
import Root from 'App/components/root'

sync(store, router)

import VueProgressiveImage from 'vue-progressive-image'
Vue.use(VueProgressiveImage)

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB5DgtOXdGEIDFUDkT9jK_EfN-UJIElU_0',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
})

const app = new Vue({
  router,
  store,
  ...Root
})

export { app, router, store }
