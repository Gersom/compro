// IMPORT MODULES NPM
  import axios from 'axios'

// IMPORT TYPES VAR
  import * as ajax from 'store/modules/ajax/types'
  import * as typesLocal from './types'
  let types = Object.assign({}, ajax, typesLocal)

// FUNCTIONS OF ACTIONS
  export const requestNews = ({commit, dispatch, state, rootState}) => {
    let axiosConfig = {
      method: 'get',
      url: `${window.API_URL}/news`,
      headers: {
        'Content-Type': 'application/json'
      }
    }

    if (rootState.route.query.tag) {
      axiosConfig.params = {
        tag: rootState.route.query.tag
      }
    }

    commit(types.BEGIN_AJAX_CALL)
    axios(axiosConfig)
    .then(({ data }) => {
      commit(types.FILL_NEWS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestRelatedNotice = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/news/${rootState.route.params.noticeSlug}/related?max_others=3`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(({ data }) => {
      commit(types.RELATED_NOTICE, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestThreeNews = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/news?max_elements=3`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(({ data }) => {
      commit(types.FILL_NEWS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestNoticeProfile = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/news/${rootState.route.params.noticeSlug}`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(({ data }) => {
      commit(types.FILL_NOTICE_PROFILE, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestPartners = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/partners`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(({ data }) => {
      commit(types.FILL_PARTNERS, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }

  export const requestAdvert = ({commit, dispatch, state, rootState}) => {
    commit(types.BEGIN_AJAX_CALL)

    axios({
      method: 'get',
      url: `${window.API_URL}/advert`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(({ data }) => {
      commit(types.FILL_ADVERT, data.data)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(({response}) => {
      commit(types.AJAX_CALL_ERROR)
    })
  }
