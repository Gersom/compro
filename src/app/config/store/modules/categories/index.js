import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: [
    {
      cover_image: '',
      id: 1,
      image: 'comproentacna.com/defaults/category.png',
      name: 'Moda y Belleza',
      slug: 'moda-y-belleza',
      categories: [
        {
          business_category_id: 1,
          id: 1,
          image: 'comproentacna.com/defaults/category.png',
          name: 'Ropa',
          slug: 'ropa'
        }
      ]
    },
    {
      cover_image: '',
      id: 1,
      image: 'comproentacna.com/defaults/category.png',
      name: 'Moda y Belleza',
      slug: 'moda-y-belleza',
      categories: [
        {
          business_category_id: 1,
          id: 1,
          image: 'comproentacna.com/defaults/category.png',
          name: 'Ropa',
          slug: 'ropa'
        }
      ]
    },
    {
      cover_image: '',
      id: 1,
      image: 'comproentacna.com/defaults/category.png',
      name: 'Moda y Belleza',
      slug: 'moda-y-belleza',
      categories: [
        {
          business_category_id: 1,
          id: 1,
          image: 'comproentacna.com/defaults/category.png',
          name: 'Ropa',
          slug: 'ropa'
        }
      ]
    },
    {
      cover_image: '',
      id: 1,
      image: 'comproentacna.com/defaults/category.png',
      name: 'Moda y Belleza',
      slug: 'moda-y-belleza',
      categories: [
        {
          business_category_id: 1,
          id: 1,
          image: 'comproentacna.com/defaults/category.png',
          name: 'Ropa',
          slug: 'ropa'
        }
      ]
    }
  ]
}

const mutations = {
  [types.RECEIVE_CATEGORIES] (state, categories) {
    state.all = categories
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
