import axios from 'axios'
import {camelizeKeys} from 'humps'

import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const loadItemDetails = ({commit, dispatch, state}) => {
  commit(types.BEGIN_AJAX_CALL)

  axios.get(`${window.API_URL}/items/${state.idSelect}`)
    .then(({data}) => {
      commit(types.RECEIVE_ITEM_DETAILS, data.data)
      commit(types.FILL_PHOTOS, data.data.photos)
      commit(types.FILL_PRICE, data.data.prices)
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}

export const fillPhotosItemDetails = ({commit}, param) => {
  commit(types.FILL_PHOTOS, param)
}
export const fillPriceItemDetails = ({commit}, param) => {
  commit(types.FILL_PRICE, param)
}

export const updateIdItem = ({commit}, param) => {
  commit(types.UPDATE_ID_ITEM, param.id)
  commit(types.UPDATE_IDEX_ITEM, param.index)
}

export const updateActionsItem = ({commit, dispatch, state, rootState}) => {
  if ((rootState.items.all.length - 1) > state.indexSelect) {
    commit(types.ENABLE_NEXT_ITEM)
  } else {
    commit(types.DISABLE_NEXT_ITEM)
  }

  if (state.indexSelect > 0) {
    commit(types.ENABLE_PREVIUS_ITEM)
  } else {
    commit(types.DISABLE_PREVIUS_ITEM)
  }
}

export const nextIdItem = ({commit, dispatch, state, rootState}) => {
  if ((rootState.items.all.length - 1) >= state.indexSelect) {
    commit(types.UPDATE_ID_ITEM, rootState.items.all[state.indexSelect + 1].id)
    commit(types.UPDATE_IDEX_ITEM, state.indexSelect + 1)
    commit(types.ENABLE_PREVIUS_ITEM)
    if ((rootState.items.all.length - 1) === state.indexSelect) {
      commit(types.DISABLE_NEXT_ITEM)
    }
  }
}

export const previousIdItem = ({commit, dispatch, state, rootState}) => {
  if (state.indexSelect >= 0) {
    commit(types.UPDATE_ID_ITEM, rootState.items.all[state.indexSelect - 1].id)
    commit(types.UPDATE_IDEX_ITEM, state.indexSelect - 1)
    commit(types.ENABLE_NEXT_ITEM)
    if (state.indexSelect === 0) {
      commit(types.DISABLE_PREVIUS_ITEM)
    }
  }
}

export const updateAttributes = ({commit, dispatch, state, rootState}, url) => {
  commit(types.BEGIN_AJAX_CALL)

  axios.get(`${window.API_URL}/items/${state.idSelect}?${url}`)
    .then(({data}) => {
      commit(types.UPDATE_ATTRIBUTES, camelizeKeys(data.data.attributes))
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}

export const fillAttributes = ({commit, dispatch, state, rootState}, elements) => {
  commit(types.UPDATE_ATTRIBUTES, elements)
}

export const loadInventory = ({commit, dispatch, state, rootState}, parameter) => {
  let urlAttr = ''
  for (var i = 0; i < parameter.attrSelected.length; i++) {
    urlAttr += state.item.attributes[i].type + '_id=' + parameter.attrSelected[i]

    if (i === parameter.attrSelected.length - 1) {
      break
    } else {
      urlAttr += '&'
    }
  }

  commit(types.BEGIN_AJAX_CALL)
  axios.get(`${window.API_URL}/items/${parameter.idItem}/inventories?${urlAttr}`)
    .then(({data}) => {
      commit(types.UPDATE_INVENTORY, camelizeKeys(data.data))
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}

export const visibleItemDetail = ({commit}) => {
  commit(types.VISIBLE_MODAL)
}
export const hiddenItemDetail = ({commit}) => {
  commit(types.HIDDEN_MODAL)
}
