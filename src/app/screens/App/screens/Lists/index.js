import Screen from './components/Screen'

export default {
  path: '/listas/:listCode',
  name: 'Lists',
  component: Screen
}
