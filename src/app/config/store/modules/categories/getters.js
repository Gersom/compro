export const categoriesAll = (state) => {
  // return categories.all
  let tmp = state.all

  for (let i = 0; i < tmp.length; i++) {
    for (let j = 0; j < tmp[i].categories.length; j++) {
      tmp[i].categories[j].bcSlug = tmp[i].slug
    }
  }

  return tmp
}

export const categoriesSlug = (state) => {
  let tmp = state.all

  for (let i = 0; i < tmp.length; i++) {
    for (let j = 0; j < tmp[i].categories.length; j++) {
      tmp[i].categories[j].bcSlug = tmp[i].slug
    }
  }

  return tmp
}

export const categorySelected = (state, getters, rootState) => {
  let tmp = []
  if (state.all[0] !== undefined) {
    tmp = state.all.find((bc) => {
      if (bc.slug === rootState.route.params.business_category) {
        return true
      } else {
        return false
      }
    })
    if (tmp) {
      tmp = tmp.categories.find((c) => {
        if (c.slug === rootState.route.params.category) {
          return true
        } else {
          return false
        }
      })
    }
  }

  return tmp
}

export const businessCategorySelected = (state, getters, rootState) => {
  return state.all.find((bc) => {
    if (bc.slug === rootState.route.params.business_category) {
      return true
    } else {
      return false
    }
  })
}
