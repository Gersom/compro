import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const enableModal = ({ commit }) => {
  commit(types.ENABLE_MODAL)
}
export const disableModal = ({ commit }) => {
  commit(types.DISABLE_MODAL)
}

export const enableShoppingCart = ({ commit }) => {
  commit(types.ENABLE_SHOPPING_CART)
}
export const disableShoppingCart = ({ commit }) => {
  commit(types.DISABLE_SHOPPING_CART)
}
