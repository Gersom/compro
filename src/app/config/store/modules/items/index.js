import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: [],
  searchQuery: '',

  page: {
    current: 1,
    total: 100,
    elements: 10
  },

  range: {
    min: 0,
    max: 999999
  }
}

const mutations = {
  [types.RECEIVE_ITEMS] (state, items) {
    state.all = items
  },
  [types.CLEAN_ITEMS] (state, items) {
    state.all = []
  },
  [types.ADD_ITEMS] (state, items) {
    state.all = state.all.concat(items)
  },

  [types.UPDATE_PAGE] (state, jsonToSave) {
    state.page.current = jsonToSave.current
    state.page.total = jsonToSave.total
    state.page.elements = jsonToSave.elements
  },

  [types.UPDATE_RANGE_MIN] (state, min) {
    state.range.min = min
  },
  [types.UPDATE_RANGE_MAX] (state, max) {
    state.range.max = max
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
