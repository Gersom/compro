// import {Promise} from 'es6-promise'
import axios from 'axios'
import {decamelizeKeys} from 'humps'

import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const clearCart = ({ commit }) => {
  commit(types.CLEAR_CART)
}

export const addToCart = ({ commit, state, rootState }, item) => {
  const recordProduct = state.added.find((element, index) => {
    return element.product.id === item.product.id
  })

  if (recordProduct) {
    let indexProduct = state.added.indexOf(recordProduct)

    const recordFeature = recordProduct.feature.find((ele) => {
      return ele.inventory.inventoryId === item.feature[0].inventory.inventoryId
    })

    if (recordFeature) {
      let indexFeature = recordProduct.feature.indexOf(recordFeature)

      commit(types.INCREASE_QUANTITY, {
        indexProduct: indexProduct,
        indexFeature: indexFeature,
        quantity: item.feature[0].inventory.quantity
      })

      let p
      const ps = rootState.itemDetails.inventory.prices
      for (let i = 0; i < ps.length; i++) {
        if (ps[i].purchaseVolumen <= recordProduct.feature[indexFeature].inventory.quantity) {
          p = ps[i]
        }
      }

      commit(types.UPDATE_PRICE_ID_CART, {
        indexProduct: indexProduct,
        indexFeature: indexFeature,
        priceId: parseInt(p.id)
      })
      commit(types.UPDATE_PRICE_CART, {
        indexProduct: indexProduct,
        indexFeature: indexFeature,
        price: {
          currencyId: p.currencyId, value: parseFloat(p.value)
        }
      })
      commit(types.UPDATE_ATTRIBUTES_CART, {
        indexProduct: indexProduct,
        indexFeature: indexFeature,
        attr: item.feature[0].attributes
      })
    } else {
      commit(types.ADD_NEW_FEATURE, {
        indexProduct: indexProduct,
        feature: item.feature[0]
      })
    }
  } else {
    commit(types.ADD_NEW_ITEM, item)
  }
}

export const removeFeatureCart = ({ commit, state }, inventoryId) => {
  let product
  let feature

  for (let i = 0; i < state.added.length; i++) {
    for (let j = 0; j < state.added[i].feature.length; j++) {
      if (state.added[i].feature[j].inventory.inventoryId === inventoryId) {
        product = i
        feature = j
      }
    }
  }

  commit(types.REMOVE_FEATURE_CART, {
    indexProduct: product,
    indexFeature: feature
  })

  if (state.added[product].feature.length === 0) {
    commit(types.REMOVE_PRODUCT_CART, product)
  }
}

export const removeProductCart = ({ commit, state }, productId) => {
  const indexProduct = state.added.findIndex((element) => {
    return `${element.product.id}` === `${productId}`
  })

  if (indexProduct > -1) {
    commit(types.REMOVE_PRODUCT_CART, indexProduct)
  }
}

// async
export const checkout = ({ commit, state }, jsonSubmit) => {
  const savedCartItems = state.added

  commit(types.CHECKOUT_REQUEST)
  commit(types.BEGIN_AJAX_CALL)

  axios({
    method: 'post',
    url: `${window.API_URL}/orders`,
    data: decamelizeKeys(jsonSubmit),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(({ data }) => {
    commit(types.AJAX_CALL_SUCCESS)
  })
  .catch(() => {
    commit(types.AJAX_CALL_ERROR)
    commit(types.CHECKOUT_FAILURE, savedCartItems)
  })
}

export const changedQuantity = ({ commit, state }, jsonSubmit) => {
  commit(types.CHANGE_QUANTITY, {
    indexProduct: jsonSubmit.indexProduct,
    indexFeature: jsonSubmit.indexFeature,
    value: jsonSubmit.value
  })
}
