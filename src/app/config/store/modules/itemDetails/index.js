import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  item: {
    // id: '',
    name: '',
    description: '',
    companyName: '',
    attributes: []
  },
  inventory: {
    companyId: null,
    photos: [{url: ''}],
    prices: []
  },
  idSelect: false,
  indexSelect: false,
  modal: false,
  nextItem: false,
  previousItem: false
}

const mutations = {
  [types.RECEIVE_ITEM_DETAILS] (state, item) {
    state.item = item
  },
  [types.FILL_PRICE] (state, param) {
    state.inventory.prices = [ param ]
  },
  [types.FILL_PHOTOS] (state, params) {
    state.inventory.photos = params
  },
  [types.UPDATE_ATTRIBUTES] (state, attr) {
    state.item.attributes = attr
  },
  [types.UPDATE_INVENTORY] (state, ivnt) {
    state.inventory = ivnt
  },
  [types.UPDATE_ID_ITEM] (state, id) {
    state.idSelect = id
  },
  [types.UPDATE_IDEX_ITEM] (state, index) {
    state.indexSelect = index
  },

  [types.ENABLE_NEXT_ITEM] (state) {
    state.nextItem = true
  },
  [types.DISABLE_NEXT_ITEM] (state) {
    state.nextItem = false
  },
  [types.ENABLE_PREVIUS_ITEM] (state) {
    state.previousItem = true
  },
  [types.DISABLE_PREVIUS_ITEM] (state) {
    state.previousItem = false
  },

  [types.VISIBLE_MODAL] (state) {
    state.modal = true
    state.inventory.id = null
  },
  [types.HIDDEN_MODAL] (state) {
    state.modal = false
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
