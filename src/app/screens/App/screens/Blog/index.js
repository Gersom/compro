import Screen from './components/Screen'
import Default from './screens/Default'

import Profile from './screens/BlogProfile'

export default {
  children: [
    Default,       // path: ''
    Profile        // path: ':noticeSlug'
  ],
  component: Screen,
  path: '/blog'
}
