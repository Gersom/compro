export const printItem = (state) => {
  return state.item
}

export const printAttributes = (state) => {
  return state.item.attributes
}

export const inventory = (state) => {
  return state.inventory
}

export const itemDetailimages = (state) => {
  return state.inventory.photos
}
export const itemDetailSlug = (state, getters, rootState) => {
  if (rootState.items.all.length > 0) {
    if (state.indexSelect !== false) {
      return rootState.items.all[state.indexSelect].slug
    } else {
      return 'no-product'
    }
  } else {
    return 'no-product'
  }
}

export const prices = (state) => {
  return state.inventory.prices
}

export const itemDetailId = (state) => {
  return state.idSelect
}

export const enableNextItem = (state) => {
  return state.nextItem
}
export const enablePreviusItem = (state) => {
  return state.previousItem
}

export const modalItemDetail = (state) => {
  return state.modal
}
