import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

// const debug = process.env.NODE_ENV !== 'production'

Vue.use(Router)

const router = new Router({
  // mode: debug ? 'hash' : 'history',
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router
