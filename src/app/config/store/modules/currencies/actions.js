import axios from 'axios'
import {camelizeKeys} from 'humps'

import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const loadCurrencies = ({commit}) => {
  commit(types.BEGIN_AJAX_CALL)

  axios.get(`${window.API_URL}/currencies`)
    .then(({data}) => {
      commit(types.UPDATE_CURRENCIES, camelizeKeys(data.data))
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}

export const changeCurrencySelected = ({commit}, select) => {
  commit(types.CHANGE_CURRENCY_SELECTED, parseInt(select))
}
