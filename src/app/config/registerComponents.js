import GerButton from 'components/GerButton'
import GerLink from 'components/GerLink'
import GerLoading from 'components/GerLoading'
import GerUserConfig from 'components/GerUserConfig'

import { swiper, swiperSlide } from 'vue-awesome-swiper'

import AppFooter from 'App/shared/components/AppFooter'
import AttachImage from 'App/shared/components/AttachImage'
import AppHeader from 'App/shared/components/AppHeader'
import AppMenu from 'App/shared/components/AppMenu'
import AppTop from 'App/shared/components/AppTop'
import Btn from 'App/shared/components/Btn'
import CategoryMenu from 'App/shared/components/CategoryMenu'
import InputNumber from 'App/shared/components/InputNumber'
import ItemDetail from 'App/shared/components/ItemDetail'
import ItemList from 'App/shared/components/ItemList'
import Loading from 'App/shared/components/Loading'
import ModalCart from 'App/shared/components/ModalCart'
import OrderItem from 'App/shared/components/OrderItem'
import OrderList from 'App/shared/components/OrderList'
import Price from 'App/shared/components/Price'
import ShoppingCart from 'App/shared/components/ShoppingCart'
import ShowCategory from 'App/shared/components/ShowCategory'
import Subscription from 'App/shared/components/Subscription'
import PanelGrid from 'App/shared/components/PanelGrid'

export default function registerComponents (Vue) {
  Vue.component('ger-button', GerButton)
  Vue.component('ger-link', GerLink)
  Vue.component('ger-loading', GerLoading)
  Vue.component('ger-user-config', GerUserConfig)

  Vue.component('swiper', swiper)
  Vue.component('swiper-slide', swiperSlide)

  Vue.component('attach-image', AttachImage)
  Vue.component('app-footer', AppFooter)
  Vue.component('app-header', AppHeader)
  Vue.component('app-menu', AppMenu)
  Vue.component('app-top', AppTop)
  Vue.component('btn', Btn)
  Vue.component('category-menu', CategoryMenu)
  Vue.component('input-number', InputNumber)
  Vue.component('item-detail', ItemDetail)
  Vue.component('item-list', ItemList)
  Vue.component('loading', Loading)
  Vue.component('modal-cart', ModalCart)
  Vue.component('order-item', OrderItem)
  Vue.component('order-list', OrderList)
  Vue.component('price', Price)
  Vue.component('shopping-cart', ShoppingCart)
  Vue.component('show-category', ShowCategory)
  Vue.component('subscription', Subscription)
  Vue.component('panel-grid', PanelGrid)
}
