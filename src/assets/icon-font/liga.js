/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
    'use strict';
    function supportsProperty(p) {
        var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
            i,
            div = document.createElement('div'),
            ret = p in div.style;
        if (!ret) {
            p = p.charAt(0).toUpperCase() + p.substr(1);
            for (i = 0; i < prefixes.length; i += 1) {
                ret = prefixes[i] + p in div.style;
                if (ret) {
                    break;
                }
            }
        }
        return ret;
    }
    var icons;
    if (!supportsProperty('fontFeatureSettings')) {
        icons = {
            'heart': '&#xe911;',
            'tag2': '&#xe900;',
            'location': '&#xe901;',
            'shopping-cart': '&#xe902;',
            'coins': '&#xe903;',
            'phone': '&#xe904;',
            'mail': '&#xe905;',
            'tag': '&#xe906;',
            'arrow-left': '&#xe907;',
            'calendar': '&#xe908;',
            'clock': '&#xe909;',
            'eye': '&#xe90a;',
            'facebook': '&#xe90b;',
            'right': '&#xe90c;',
            'google-plus': '&#xe90d;',
            'down': '&#xe90e;',
            'left-circle': '&#xe90f;',
            'right-circle': '&#xe910;',
            'category': '&#xe912;',
            'check': '&#xe913;',
            'close-circle': '&#xe914;',
            'close': '&#xe915;',
            'tacna': '&#xe916;',
            'mail-white': '&#xe917;',
            'coins-white': '&#xe918;',
            'tag-white': '&#xe919;',
            'search': '&#xe91a;',
            'instagram': '&#xe91b;',
            'map-marker': '&#xe91c;',
            'mail-circle': '&#xe91d;',
            'menu': '&#xe91e;',
            'padlock': '&#xe91f;',
            'play-button': '&#xe920;',
            'shopping-list': '&#xe921;',
            'store': '&#xe922;',
            'phone-circle': '&#xe923;',
            'gear': '&#xe924;',
            'twitter': '&#xe925;',
            'location-circle': '&#xe926;',
            'user': '&#xe927;',
            'youtube': '&#xe928;',
          '0': 0
        };
        delete icons['0'];
        window.icomoonLiga = function (els) {
            var classes,
                el,
                i,
                innerHTML,
                key;
            els = els || document.getElementsByTagName('*');
            if (!els.length) {
                els = [els];
            }
            for (i = 0; ; i += 1) {
                el = els[i];
                if (!el) {
                    break;
                }
                classes = el.className;
                if (/u-icon/.test(classes)) {
                    innerHTML = el.innerHTML;
                    if (innerHTML && innerHTML.length > 1) {
                        for (key in icons) {
                            if (icons.hasOwnProperty(key)) {
                                innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
                            }
                        }
                        el.innerHTML = innerHTML;
                    }
                }
            }
        };
        window.icomoonLiga();
    }
}());
