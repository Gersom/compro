import * as defaults from './defaults'

export const userData = (state) => {
  let empty = true
  for (empty in state.data) { empty = false; break }

  if (!empty) { return state.data }
  return defaults.USER
}
