import Screen from './components/Screen'
import Default from './screens/Default'

export default {
  children: [
    Default         // path: ''
  ],
  component: Screen,
  path: '/tiendas'
}
