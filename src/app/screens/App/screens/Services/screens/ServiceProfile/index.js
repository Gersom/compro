import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'ServiceProfile',
  path: ':serviceSlug'
}
