import Screen from './components/Screen'

export default {
  component: Screen,
  name: 'ShopProfile',
  path: '/:shopSlug'
}
