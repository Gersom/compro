export const items = (state) => {
  // let filtered = state.all.filter((item) => {
  //   if (parseFloat(item.price.value) >= state.range.min) {
  //     if (parseFloat(item.price.value) <= state.range.max) {
  //       return true
  //     } else {
  //       return false
  //     }
  //   } else {
  //     return false
  //   }
  // })
  //
  // return filtered

  return state.all
}

export const filterItemsBusinessCategories = (state, getters, rootState) => {
  let filtered = state.all.filter((value) => {
    if (rootState.route) {
      if (value.businessCategorySlug === rootState.route.params.business_category) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  })

  // filtered = filtered.filter((item) => {
  //   if (parseFloat(item.price.value) >= state.range.min) {
  //     if (parseFloat(item.price.value) <= state.range.max) {
  //       return true
  //     } else {
  //       return false
  //     }
  //   } else {
  //     return false
  //   }
  // })

  return filtered
}

export const filterItemsCategories = (state, getters, rootState) => {
  function forCAT (value) {
    if (rootState.route) {
      if (value.businessCategorySlug === rootState.route.params.business_category) {
        if (value.categorySlug === rootState.route.params.category) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    } else {
      return false
    }
  }

  let filtered = state.all.filter(forCAT)
  // filtered = filtered.filter((item) => {
  //   if (parseFloat(item.price.value) >= state.range.min) {
  //     if (parseFloat(item.price.value) <= state.range.max) {
  //       return true
  //     } else {
  //       return false
  //     }
  //   } else {
  //     return false
  //   }
  // })

  return filtered
}

export const minMaxPrices = (state) => {
  let min = 99999999999
  let max = 0

  state.all.forEach((item) => {
    if (parseInt(item.price.value) < min) {
      min = parseInt(item.price.value)
    }
    if (parseInt(item.price.value) > max) {
      max = parseInt(item.price.value)
    }
  })

  return {
    min: min,
    max: max
  }
}

export const nextPage = (state) => {
  return state.page.total > (state.page.elements * state.page.current)
}
